package org.nikaspran.dpd.display;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixComponent;

public class MatrixDisplay {

	/**
	 * Display matrix:
	 * <ul>
	 * <li><b>blue</b> - negative</li>
	 * <li><b>red</b> - positive</li>
	 * <li><b>black</b> - zero</li>
	 * </ul>
	 * @param matrix
	 * @param title
	 */
	public static void show(DenseMatrix64F matrix, String title) {
		JFrame frame = new JFrame(title);

		int width = 1024;
		int height = 900;

		if (matrix.numRows > matrix.numCols) {
			width = width * matrix.numCols / matrix.numRows;
		} else {
			height = height * matrix.numRows / matrix.numCols;
		}

		MatrixComponent panel = new MatrixComponent(width, height);
		panel.setMatrix(matrix);

		frame.add(panel, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
	}

}
