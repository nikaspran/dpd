package org.nikaspran.dpd.display;

import java.awt.Color;
import java.awt.Font;

import org.jgraph.graph.AttributeMap;
import org.jgraph.graph.GraphConstants;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.graph.DefaultEdge;
import org.nikaspran.dpd.model.JavaGraph;
import org.nikaspran.dpd.model.Node;

public class JavaGraphAdapter extends JGraphModelAdapter<Node, DefaultEdge> {
	private static final long serialVersionUID = -2329678325934085443L;
	private static final CellFactory<Node, DefaultEdge> CELL_FACTORY = new JavaGraphCellFactory();

	public JavaGraphAdapter(JavaGraph javaGraph) {
		super(javaGraph, createDefaultVertexAttributes(), getDefaultJavaEdgeAttributes(javaGraph), CELL_FACTORY);
	}

	private static AttributeMap getDefaultJavaEdgeAttributes(JavaGraph javaGraph) {
		AttributeMap map = new AttributeMap();

		GraphConstants.setEndSize(map, 10);

		GraphConstants.setForeground(map, Color.decode("#25507C"));
		GraphConstants.setFont(map, GraphConstants.DEFAULTFONT.deriveFont(Font.BOLD, 12));
		GraphConstants.setLineColor(map, Color.decode("#7AA1E6"));
		return map;
	}
}
