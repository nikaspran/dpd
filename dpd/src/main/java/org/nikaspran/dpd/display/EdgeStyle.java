package org.nikaspran.dpd.display;

public class EdgeStyle {

	private final int lineBegin;
	private final boolean beginFill;
	private final int lineEnd;
	private final boolean endFill;
	private final float[] dashPattern;

	public EdgeStyle(int lineBegin, boolean beginFill, int lineEnd, boolean endFill) {
		this(lineBegin, beginFill, lineEnd, endFill, null);
	}

	public EdgeStyle(int lineBegin, boolean beginFill, int lineEnd, boolean endFill, float[] dashPattern) {
		super();
		this.lineBegin = lineBegin;
		this.beginFill = beginFill;
		this.lineEnd = lineEnd;
		this.endFill = endFill;
		this.dashPattern = dashPattern;
	}

	public int getLineBegin() {
		return lineBegin;
	}

	public int getLineEnd() {
		return lineEnd;
	}

	public boolean isBeginFill() {
		return beginFill;
	}

	public boolean isEndFill() {
		return endFill;
	}

	public float[] getDashPattern() {
		return dashPattern;
	}

}
