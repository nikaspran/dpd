package org.nikaspran.dpd.display;

import java.util.Map;

import org.jgraph.graph.AttributeMap;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;
import org.jgrapht.ext.JGraphModelAdapter.DefaultCellFactory;
import org.jgrapht.graph.DefaultEdge;
import org.nikaspran.dpd.model.Node;
import org.nikaspran.dpd.model.edges.AssociationEdge;
import org.nikaspran.dpd.model.edges.CollectionEdge;
import org.nikaspran.dpd.model.edges.FieldEdge;
import org.nikaspran.dpd.model.edges.GeneralizationEdge;
import org.nikaspran.dpd.model.edges.InheritanceEdge;
import org.nikaspran.dpd.model.edges.InterfaceEdge;

import com.google.common.collect.Maps;

public class JavaGraphCellFactory extends DefaultCellFactory<Node, DefaultEdge> {
	private static final long serialVersionUID = 6662724131144729309L;
	private final Map<Class<? extends DefaultEdge>, EdgeStyle> edgeStyles = Maps.newHashMap();

	@Override
	public DefaultGraphCell createVertexCell(Node node) {
		DefaultGraphCell vertex = super.createVertexCell(node);
		GraphConstants.setResize(vertex.getAttributes(), true);
		return vertex;
	}

	@Override
	public org.jgraph.graph.DefaultEdge createEdgeCell(DefaultEdge modelEdge) {
		org.jgraph.graph.DefaultEdge displayEdge = super.createEdgeCell(modelEdge);

		EdgeStyle style = getStyle(modelEdge);
		AttributeMap attributes = displayEdge.getAttributes();

		GraphConstants.setLineBegin(attributes, style.getLineBegin());
		GraphConstants.setBeginFill(attributes, style.isBeginFill());
		GraphConstants.setLineEnd(attributes, style.getLineEnd());
		GraphConstants.setEndFill(attributes, style.isEndFill());
		if (style.getDashPattern() != null) {
			GraphConstants.setDashPattern(attributes, style.getDashPattern());
		}

		return displayEdge;
	}

	public void addStyle(Class<? extends DefaultEdge> edgeClass, EdgeStyle style) {
		edgeStyles.put(edgeClass, style);
	}

	public EdgeStyle getStyle(DefaultEdge edge) {
		return edgeStyles.get(edge.getClass());
	}

	public JavaGraphCellFactory() {
		float[] simpleDash = new float[] { 5, 5 };

		addStyle(DefaultEdge.class, new EdgeStyle(GraphConstants.ARROW_NONE, true, GraphConstants.ARROW_SIMPLE, true, null));
		addStyle(AssociationEdge.class, new EdgeStyle(GraphConstants.ARROW_NONE, true, GraphConstants.ARROW_SIMPLE, true, simpleDash));

		addStyle(FieldEdge.class, new EdgeStyle(GraphConstants.ARROW_DIAMOND, false, GraphConstants.ARROW_NONE, true));
		addStyle(CollectionEdge.class, new EdgeStyle(GraphConstants.ARROW_DIAMOND, false, GraphConstants.ARROW_DOUBLELINE, true));
//		addStyle(AggregationEdge.class, new EdgeStyle(GraphConstants.ARROW_DIAMOND, false, GraphConstants.ARROW_NONE, true));
//		addStyle(CompositionEdge.class, new EdgeStyle(GraphConstants.ARROW_DIAMOND, true, GraphConstants.ARROW_NONE, true));

		addStyle(GeneralizationEdge.class, new EdgeStyle(GraphConstants.ARROW_NONE, true, GraphConstants.ARROW_TECHNICAL, true));
		addStyle(InheritanceEdge.class, new EdgeStyle(GraphConstants.ARROW_NONE, true, GraphConstants.ARROW_TECHNICAL, false));
		addStyle(InterfaceEdge.class, new EdgeStyle(GraphConstants.ARROW_NONE, true, GraphConstants.ARROW_TECHNICAL, false, simpleDash));
	}

}
