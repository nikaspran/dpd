package org.nikaspran.dpd.model;

import java.util.Map;

import org.nikaspran.dpd.parser.FieldDescriptor;
import org.nikaspran.dpd.parser.MethodDescriptor;

import com.google.common.collect.Maps;

/**
 * @author Nikas
 * 
 */
public class Node {

	private String name;
	private boolean abstractType;
	private boolean invokesAbstractMethods;
	private boolean anonymous;
	private boolean publicType;
	private int publicMethodCount;
	private final Map<String, FieldDescriptor> fields = Maps.newHashMap();
	private final Map<String, MethodDescriptor> methods = Maps.newHashMap();

	public Node(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public boolean isAbstractType() {
		return abstractType;
	}

	public void setAbstractType(boolean abstractType) {
		this.abstractType = abstractType;
	}

	public boolean isInvokesAbstractMethods() {
		return invokesAbstractMethods;
	}

	public void setInvokesAbstractMethods(boolean invokesAbstractMethods) {
		this.invokesAbstractMethods = invokesAbstractMethods;
	}

	public int getPublicMethodCount() {
		return publicMethodCount;
	}

	public void setPublicMethodCount(int methodCount) {
		this.publicMethodCount = methodCount;
	}

	public Map<String, MethodDescriptor> getMethods() {
		return methods;
	}

	public Map<String, FieldDescriptor> getFields() {
		return fields;
	}

	public boolean isAnonymous() {
		return anonymous;
	}

	public void setAnonymous(boolean anonymous) {
		this.anonymous = anonymous;
	}

	public boolean isPublicType() {
		return publicType;
	}

	public void setPublicType(boolean publicType) {
		this.publicType = publicType;
	}

}
