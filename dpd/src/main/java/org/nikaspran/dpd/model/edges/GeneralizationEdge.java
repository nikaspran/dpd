package org.nikaspran.dpd.model.edges;

import org.jgrapht.graph.DefaultEdge;

public class GeneralizationEdge extends DefaultEdge {
	private static final long serialVersionUID = -1929537905603690114L;

	@Override
	public String toString() {
		return "inherits from";
	}
}
