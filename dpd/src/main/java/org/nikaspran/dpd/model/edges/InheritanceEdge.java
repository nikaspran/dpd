package org.nikaspran.dpd.model.edges;

public class InheritanceEdge extends GeneralizationEdge {
	private static final long serialVersionUID = -7369762837488657476L;

	@Override
	public String toString() {
		return "is a";
	}
}
