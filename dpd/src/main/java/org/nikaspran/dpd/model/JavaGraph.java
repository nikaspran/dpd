package org.nikaspran.dpd.model;

import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedPseudograph;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;

public class JavaGraph extends DirectedPseudograph<Node, DefaultEdge> {
	private static final long serialVersionUID = -4939101275799962082L;
	private static final Set<String> PRIMITIVE_TYPES = ImmutableSet.of("void", "boolean", "char", "byte", "short", "int", "float", "long", "double");

	private final Map<String, Node> nodes = Maps.newHashMap();
	private String name;

	public JavaGraph(String name) {
		super(DefaultEdge.class);
		this.name = name;
	}

	public Map<String, Node> getNodes() {
		return nodes;
	}

	public boolean connect(String source, String target, DefaultEdge edge) {
		Node sourceNode = createOrGet(source);
		Node targetNode = createOrGet(target);

		if (sourceNode == null || targetNode == null) {
			return false;
		}

		return addEdge(sourceNode, targetNode, edge);
	}

	public boolean connectIfNot(String source, String target, DefaultEdge newEdge) {
		Node sourceNode = createOrGet(source);
		Node targetNode = createOrGet(target);
		Set<DefaultEdge> edges = getAllEdges(sourceNode, targetNode);
		if (edges == null) {
			return false;
		}
		for (DefaultEdge edge : edges) {
			if (edge.getClass().equals(newEdge.getClass()) && edge.toString().equals(newEdge.toString())) { // TODO: something more robust
				return false;
			}
		}

		return addEdge(sourceNode, targetNode, newEdge);
	}

	public <T extends DefaultEdge> T get(String source, String target, Class<T> edgeType) {
		return get(get(source), get(target), edgeType);
	}

	@SuppressWarnings("unchecked")
	public <T extends DefaultEdge> T get(Node source, Node target, Class<T> edgeType) {
		for (DefaultEdge edge : getAllEdges(source, target)) {
			if (edge.getClass().isAssignableFrom(edgeType)) {
				return (T) edge;
			}
		}
		return null;
	}

	public Node createOrGet(String uniqueId) {
		if (!includeInGraph(uniqueId)) {
			return null;
		}
		Node node = nodes.get(uniqueId);
		if (node == null) {
			node = new Node(uniqueId);
			nodes.put(uniqueId, node);
			addVertex(node);
		}
		return node;
	}

	public Node get(String uniqueId) {
		return nodes.get(uniqueId);
	}

	public int getEdgeCount(String nodeName, Class<? extends DefaultEdge> edgeClass) {
		Node node = nodes.get(nodeName);
		if (node == null) {
			throw new IllegalArgumentException("Node " + nodeName + " does not exist in the graph");
		}

		int edgeCount = 0;
		for (DefaultEdge edge : edgesOf(node)) {
			if (edgeClass.isInstance(edge)) {
				edgeCount++;
			}
		}
		return edgeCount;
	}

	@SuppressWarnings("unchecked")
	public int[] getEdgeCounts(String nodeName, Class<?>... edgeClasses) {
		int[] edgeCounts = new int[edgeClasses.length];
		for (int i = 0; i < edgeClasses.length; i++) {
			edgeCounts[i] = getEdgeCount(nodeName, (Class<? extends DefaultEdge>) edgeClasses[i]);
		}
		return edgeCounts;

	}

	private boolean includeInGraph(String node) {
		if ("java.util.Collection".equals(node)) {
			return true;
		}

		return !StringUtils.startsWith(node, "java.") && !PRIMITIVE_TYPES.contains(node);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
