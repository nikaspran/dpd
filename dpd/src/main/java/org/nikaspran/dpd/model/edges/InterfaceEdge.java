package org.nikaspran.dpd.model.edges;

public class InterfaceEdge extends GeneralizationEdge {
	private static final long serialVersionUID = 1177182992802184782L;

	@Override
	public String toString() {
		return "implements";
	}
}
