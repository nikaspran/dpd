package org.nikaspran.dpd.model.edges;

import org.jgrapht.graph.DefaultEdge;

public class AssociationEdge extends DefaultEdge {
	private static final long serialVersionUID = 3614793116789688656L;

	private final String methodName;

	public AssociationEdge(String methodName) {
		super();
		this.methodName = methodName;
	}

	@Override
	public String toString() {
		return methodName;
	}

}
