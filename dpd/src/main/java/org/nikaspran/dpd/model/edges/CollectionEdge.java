package org.nikaspran.dpd.model.edges;

public class CollectionEdge extends FieldEdge {
	private static final long serialVersionUID = 9161771022393962550L;

	public CollectionEdge(String fieldName) {
		super(fieldName);
	}

	@Override
	public String toString() {
		return "has N";
	}

}
