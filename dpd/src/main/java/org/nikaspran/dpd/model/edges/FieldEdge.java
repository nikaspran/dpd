package org.nikaspran.dpd.model.edges;

import org.jgrapht.graph.DefaultEdge;

public class FieldEdge extends DefaultEdge {
	private static final long serialVersionUID = -5052892915821309253L;

	private final String fieldName;

	public FieldEdge(String fieldName) {
		super();
		this.fieldName = fieldName;
	}

	public String getFieldName() {
		return fieldName;
	}

	@Override
	public String toString() {
		return "has a";
	}
}
