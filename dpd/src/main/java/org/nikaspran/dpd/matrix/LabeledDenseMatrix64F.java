package org.nikaspran.dpd.matrix;

import org.ejml.data.DenseMatrix64F;

public class LabeledDenseMatrix64F extends DenseMatrix64F {
	private static final long serialVersionUID = -7499414303500821362L;

	private String[] rowLabels;
	private String[] colLabels;

	public LabeledDenseMatrix64F(int numRows, int numCols) {
		super(numRows, numCols);
		rowLabels = new String[numRows];
		colLabels = new String[numCols];
	}

	public LabeledDenseMatrix64F(DenseMatrix64F other) {
		super(other);
		rowLabels = new String[numRows];
		colLabels = new String[numCols];
	}

	public String getRowLabel(int row) {
		return rowLabels[row];
	}

	public String getColLabel(int col) {
		return colLabels[col];
	}

	public void setRowLabel(int row, String label) {
		rowLabels[row] = label;
	}

	public void setColLabel(int col, String label) {
		colLabels[col] = label;
	}

	@Override
	public LabeledDenseMatrix64F copy() {
		LabeledDenseMatrix64F copy = new LabeledDenseMatrix64F(super.copy());
		copy.copyLabelsFrom(this); //TODO: do we need to clone arrays?
		return copy;
	}

	public void copyLabelsFrom(LabeledDenseMatrix64F source) {
		colLabels = source.colLabels.clone();
		rowLabels = source.rowLabels.clone();
	}

}
