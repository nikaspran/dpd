package org.nikaspran.dpd.matrix;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.ejml.simple.SimpleMatrix;

import com.google.common.collect.Lists;

public final class MatrixUtils {

	public static SimpleMatrix getNonZeroPermutations(SimpleMatrix matrix) {
		List<Integer> nonZeroIndices = findNonZeroIndices(matrix.extractVector(false, 0));
		if (CollectionUtils.isEmpty(nonZeroIndices)) {
			nonZeroIndices.add(-1);
		}
		if (matrix.numCols() == 1) {
			return toVector(nonZeroIndices);
		}
		SimpleMatrix otherPermutations = getNonZeroPermutations(matrix.extractMatrix(0, matrix.numRows(), 1, matrix.numCols()));
		SimpleMatrix result = new SimpleMatrix(nonZeroIndices.size() * otherPermutations.numRows(), otherPermutations.numCols() + 1);
		int i = 0;
		for (int nonZeroIndex : nonZeroIndices) {
			SimpleMatrix permute = new SimpleMatrix(otherPermutations.numRows(), otherPermutations.numCols() + 1);
			setCol(permute, 0, nonZeroIndex);
			permute.insertIntoThis(0, 1, otherPermutations);
			result.insertIntoThis(otherPermutations.numRows() * i++, 0, permute);
		}
		return result;
	}

	public static void setRow(SimpleMatrix matrix, int row, double value) {
		for (int col = 0; col < matrix.numCols(); col++) {
			matrix.set(row, col, value);
		}
	}

	public static void setCol(SimpleMatrix matrix, int col, double value) {
		for (int row = 0; row < matrix.numRows(); row++) {
			matrix.set(row, col, value);
		}
	}

	public static SimpleMatrix toVector(List<Integer> list) {
		SimpleMatrix matrix = new SimpleMatrix(list.size(), 1);
		int i = 0;
		for (int element : list) {
			matrix.set(i++, element);
		}
		return matrix;
	}

	public static List<Integer> findNonZeroIndices(SimpleMatrix vector) {
		List<Integer> nonZeroIndices = Lists.newArrayList();
		for (int i = 0; i < vector.getNumElements(); i++) {
			if (vector.get(i) != 0) {
				nonZeroIndices.add(i);
			}
		}
		return nonZeroIndices;
	}

	private MatrixUtils() {
	}

}
