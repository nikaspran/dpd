package org.nikaspran.dpd.detection.impl;

import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.Maps;

public class PatternMapping {

	private Map<String, String> mappings = Maps.newHashMap();
	private double nodeError;
	private double edgeError;

	public void addMapping(String patternClass, String systemClass) {
		mappings.put(patternClass, systemClass);
	}

	public Map<String, String> getMappings() {
		return mappings;
	}

	public void setMappings(Map<String, String> mappings) {
		this.mappings = mappings;
	}

	public double getNodeError() {
		return nodeError;
	}

	public void setNodeError(double nodeError) {
		this.nodeError = nodeError;
	}

	public double getEdgeError() {
		return edgeError;
	}

	public void setEdgeError(double edgeError) {
		this.edgeError = edgeError;
	}

	public double getTotalError() {
		return nodeError + edgeError;
	}

	public double getAverageError() {
		return getTotalError() / 2;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("Pattern mapping ");
		builder.append("[NE = ").append(nodeError).append(", EE = ").append(edgeError).append(", AVG = ").append(getAverageError()).append("]:\n");
		for (Entry<String, String> mapping : mappings.entrySet()) {
			builder.append('\t');
			builder.append(mapping.getKey()).append(" => ").append(mapping.getValue());
			builder.append('\n');
		}
		return builder.toString();
	}

}
