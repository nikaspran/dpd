package org.nikaspran.dpd.detection;

import java.util.Collection;

public final class DetectionUtils {

	public static final <T> T getFirst(Collection<T> collection) {
		for (T item : collection) {
			return item;
		}
		return null;
	}

	private DetectionUtils() {
	}
}
