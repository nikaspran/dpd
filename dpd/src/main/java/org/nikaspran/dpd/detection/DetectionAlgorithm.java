package org.nikaspran.dpd.detection;

import java.util.Collection;

import org.nikaspran.dpd.detection.impl.PatternMapping;
import org.nikaspran.dpd.model.JavaGraph;

public abstract class DetectionAlgorithm {

	private JavaGraph system;

	public DetectionAlgorithm(JavaGraph system) {
		super();
		this.system = system;
	}

	public abstract Collection<PatternMapping> run(JavaGraph pattern);

	public JavaGraph getSystem() {
		return system;
	}

	public void setSystem(JavaGraph system) {
		this.system = system;
	}

}
