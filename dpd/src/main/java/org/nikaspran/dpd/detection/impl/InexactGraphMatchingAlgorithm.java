package org.nikaspran.dpd.detection.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixFeatures;
import org.ejml.simple.SimpleMatrix;
import org.jgrapht.graph.DefaultEdge;
import org.nikaspran.dpd.detection.DetectionAlgorithm;
import org.nikaspran.dpd.matrix.LabeledDenseMatrix64F;
import org.nikaspran.dpd.matrix.MatrixUtils;
import org.nikaspran.dpd.model.JavaGraph;
import org.nikaspran.dpd.model.Node;
import org.nikaspran.dpd.model.edges.AssociationEdge;
import org.nikaspran.dpd.model.edges.CollectionEdge;
import org.nikaspran.dpd.model.edges.FieldEdge;
import org.nikaspran.dpd.model.edges.GeneralizationEdge;
import org.nikaspran.dpd.model.edges.InheritanceEdge;
import org.nikaspran.dpd.model.edges.InterfaceEdge;

import com.google.common.collect.ArrayTable;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;

public class InexactGraphMatchingAlgorithm extends DetectionAlgorithm {
	@SuppressWarnings("unchecked") private static final Class<? extends DefaultEdge>[] INCOMING_EDGE_VECTOR = new Class[] { GeneralizationEdge.class };
	@SuppressWarnings("unchecked") private static final Class<? extends DefaultEdge>[] OUTGOING_EDGE_VECTOR = new Class[] { FieldEdge.class, AssociationEdge.class, GeneralizationEdge.class };
	//@formatter:off
	private static final Map<Class<? extends DefaultEdge>, Class<? extends DefaultEdge>> EDGE_TYPE_MAP = ImmutableMap.<Class<? extends DefaultEdge>, Class<? extends DefaultEdge>>builder()
			.put(FieldEdge.class, FieldEdge.class)
			.put(AssociationEdge.class, AssociationEdge.class)
			.put(GeneralizationEdge.class, GeneralizationEdge.class)
			.put(CollectionEdge.class, FieldEdge.class)
			.put(InheritanceEdge.class, GeneralizationEdge.class)
			.put(InterfaceEdge.class, GeneralizationEdge.class)
			.build();
	//@formatter:on

	private final Table<Node, Node, Set<Class<? extends DefaultEdge>>> systemEdgeTable;
	private Table<Node, Node, Set<Class<? extends DefaultEdge>>> patternEdgeTable;

	private int phases = 30;

	public InexactGraphMatchingAlgorithm(JavaGraph system) {
		super(system);
		this.systemEdgeTable = ArrayTable.create(system.getNodes().values(), system.getNodes().values());
		preprocessEdgeRelations(system, systemEdgeTable);
	}

	@Override
	public Collection<PatternMapping> run(JavaGraph pattern) {
		final int patternNodeCount = pattern.getNodes().size(); //n
		patternEdgeTable = ArrayTable.create(pattern.getNodes().values(), pattern.getNodes().values());
		preprocessEdgeRelations(pattern, patternEdgeTable);
		final int systemNodeCount = getSystem().getNodes().size(); //m
		final Map<String, int[]> patternNodeVectors = getNodeVector(pattern);
		final Map<String, int[]> systemNodeVectors = getNodeVector(getSystem());

		LabeledDenseMatrix64F distanceMatrix = getDistanceMatrix(patternNodeVectors, systemNodeVectors);
		LabeledDenseMatrix64F potentialMappingMatrix = new LabeledDenseMatrix64F(patternNodeCount, systemNodeCount);
		potentialMappingMatrix.copyLabelsFrom(distanceMatrix);

		ExecutorService threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

		Set<PatternMapping> mappings = Collections.newSetFromMap(new ConcurrentHashMap<PatternMapping, Boolean>(patternNodeCount * systemNodeCount));
		for (int row = 0; row < patternNodeCount; row++) {
			int minCol = getMinRowValueColumnIndex(row, distanceMatrix, potentialMappingMatrix);
			potentialMappingMatrix.set(row, minCol, 1);
		}
		threadPool.execute(new MappingExtractor(mappings, potentialMappingMatrix.copy(), patternNodeVectors, systemNodeVectors, pattern, getSystem()));

		for (int phase = 0; phase < phases; phase++) {
			if (MatrixFeatures.isConstantVal(potentialMappingMatrix, 1, 0)) {
				break;
			}

			for (int row = 0; row < patternNodeCount; row++) {
				LabeledDenseMatrix64F checkedCellsMatrix = potentialMappingMatrix.copy();
				zeroRow(row, potentialMappingMatrix);
				int minCol = getMinRowValueColumnIndex(row, distanceMatrix, checkedCellsMatrix);

				checkedCellsMatrix.set(row, minCol, 1);
				potentialMappingMatrix.set(row, minCol, 1);
				threadPool.execute(new MappingExtractor(mappings, potentialMappingMatrix.copy(), patternNodeVectors, systemNodeVectors, pattern, getSystem()));
				potentialMappingMatrix = checkedCellsMatrix;
			}
		}
		try {
			threadPool.shutdown();
			threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return mappings;
	}

	private class MappingExtractor implements Runnable {
		private final Collection<PatternMapping> mappings;
		private final LabeledDenseMatrix64F mappingMatrix;
		private final Map<String, int[]> patternNodeVectors;
		private final Map<String, int[]> systemNodeVectors;
		private final JavaGraph pattern;
		private final JavaGraph system;

		public MappingExtractor(Collection<PatternMapping> mappings, LabeledDenseMatrix64F mappingMatrix, Map<String, int[]> patternNodeVectors, Map<String, int[]> systemNodeVectors,
				JavaGraph pattern, JavaGraph system) {
			super();
			this.mappings = mappings;
			this.mappingMatrix = mappingMatrix;
			this.patternNodeVectors = patternNodeVectors;
			this.systemNodeVectors = systemNodeVectors;
			this.pattern = pattern;
			this.system = system;
		}

		@Override
		public void run() {
			mappings.addAll(getMappings(mappingMatrix, patternNodeVectors, systemNodeVectors, pattern, system));
		}
	}

	private void preprocessEdgeRelations(JavaGraph graph, Table<Node, Node, Set<Class<? extends DefaultEdge>>> edgeTable) {
		for (Node firstClass : graph.getNodes().values()) {
			for (Node secondClass : graph.getNodes().values()) {
				Set<Class<? extends DefaultEdge>> edgeTypes = Sets.newHashSetWithExpectedSize(6);
				for (DefaultEdge edge : graph.getAllEdges(firstClass, secondClass)) {
					edgeTypes.add(edge.getClass());
				}
				edgeTable.put(firstClass, secondClass, edgeTypes);
			}
		}
	}

	private static void zeroRow(int row, DenseMatrix64F matrix) {
		for (int col = 0; col < matrix.numCols; col++) {
			matrix.set(row, col, 0);
		}
	}

	private List<PatternMapping> getMappings(LabeledDenseMatrix64F mappingMatrix, Map<String, int[]> patternNodeVectors, Map<String, int[]> systemNodeVectors, JavaGraph pattern, JavaGraph system) {
		// possibleMatchings has a row for each possible matching, with columns denoting pattern class indices and values denoting system class indices in mappingMatrix
		SimpleMatrix possibleMatchings = MatrixUtils.getNonZeroPermutations(SimpleMatrix.wrap(mappingMatrix).transpose());

		int maxNodeError = getMaxNodeDistance() * possibleMatchings.numCols();

		List<PatternMapping> validMappings = Lists.newArrayList();
		for (int matching = 0; matching < possibleMatchings.numRows(); matching++) {
			// is matching not bijective?
			if (containsDublicatesOrValue(possibleMatchings, matching, -1, systemNodeVectors.size())) {
				continue;
			}

			PatternMapping mapping = new PatternMapping();
			double nodeError = 0;
			for (int col = 0; col < possibleMatchings.numCols(); col++) {
				String patternClass = mappingMatrix.getRowLabel(col);
				String systemClass = mappingMatrix.getColLabel((int) possibleMatchings.get(matching, col));
				mapping.addMapping(patternClass, systemClass);
				nodeError += getSimpleDistance(patternNodeVectors.get(patternClass), systemNodeVectors.get(systemClass));
			}
			mapping.setNodeError(nodeError / maxNodeError);
			mapping.setEdgeError(getEdgeError(mapping.getMappings(), pattern, system));
			validMappings.add(mapping);
		}

		return validMappings;
	}

	private double getEdgeError(Map<String, String> mapping, JavaGraph pattern, JavaGraph system) {
//		if (mapping.get("org.nikaspran.dpd.patterns.decorator.Component").equals("junit.framework.Test")
//				&& mapping.get("org.nikaspran.dpd.patterns.decorator.Decorator").equals("junit.extensions.TestDecorator")
//				&& mapping.get("org.nikaspran.dpd.patterns.decorator.ConcreteDecorator").equals("junit.extensions.RepeatedTest")) {
//			System.out.println("break");
//		}

		int wrongEdges = 0;
		int totalEdges = 0;
		for (String patternClass : mapping.keySet()) {
			Node patternClassNode = pattern.get(patternClass);
			Node systemClassNode = system.get(mapping.get(patternClass));
			for (String otherPatternClass : mapping.keySet()) {
				Node otherSystemClassNode = system.get(mapping.get(otherPatternClass));

				Set<Class<? extends DefaultEdge>> systemEdgeTypes = systemEdgeTable.get(systemClassNode, otherSystemClassNode);
				Set<Class<? extends DefaultEdge>> patternEdgeTypes = patternEdgeTable.get(patternClassNode, pattern.get(otherPatternClass));

				//Gupta method
//				if (!Sets.difference(patternEdgeTypes, systemEdgeTypes).isEmpty()) {
//					wrongEdges += 1;
//					totalEdges = 1;
//				}

				//ImprovedMethod
				wrongEdges += Sets.difference(patternEdgeTypes, systemEdgeTypes).size();
				totalEdges += patternEdgeTypes.size();
			}
		}
		return wrongEdges / (double) totalEdges;
	}

	private static boolean containsDublicatesOrValue(SimpleMatrix matrix, int row, int valueToCheckFor, int maxValue) {
		boolean[] bitmap = new boolean[maxValue + 1];
		for (int col = 0; col < matrix.numCols(); col++) {
			int value = (int) matrix.get(row, col);
			if (value == valueToCheckFor || !(bitmap[value] ^= true)) {
				return true;
			}
		}
		return false;
	}

	private static int getMinRowValueColumnIndex(int row, DenseMatrix64F matrix, DenseMatrix64F notOneInThisMatrix) {
		int minColIndex = -1;
		double minValue = Double.MAX_VALUE;
		for (int col = 0; col < matrix.getNumCols(); col++) {
			double value = matrix.get(row, col);
			if (value < minValue && notOneInThisMatrix.get(row, col) != 1) {
				minColIndex = col;
				minValue = value;
			}
		}
		return minColIndex;
	}

	private static LabeledDenseMatrix64F getDistanceMatrix(Map<String, int[]> patternNodeVectors, Map<String, int[]> systemNodeVectors) {
		LabeledDenseMatrix64F result = new LabeledDenseMatrix64F(patternNodeVectors.size(), systemNodeVectors.size());
		Set<String> systemNodes = systemNodeVectors.keySet();

		int row = 0;
		for (String patternClass : patternNodeVectors.keySet()) {
			int col = 0;
			int[] patternNodeVector = patternNodeVectors.get(patternClass);
			result.setRowLabel(row, patternClass);
			for (String systemClass : systemNodes) {
				result.setColLabel(col, systemClass);
				result.set(row, col++, getEuclideanDistance(patternNodeVector, systemNodeVectors.get(systemClass)));
			}
			row++;
		}

		return result;
	}

	private static Map<String, int[]> getNodeVector(JavaGraph graph) {
		Map<String, int[]> result = Maps.newHashMap();
		for (String className : graph.getNodes().keySet()) {
			Node node = graph.get(className);

			Set<Class<? extends DefaultEdge>> incomingEdges = getEdgeTypes(graph.incomingEdgesOf(node));
			Set<Class<? extends DefaultEdge>> outgoingEdges = getEdgeTypes(graph.outgoingEdgesOf(node));

			int[] vector = new int[getMaxNodeDistance()];
			int i = 0;
			for (Class<? extends DefaultEdge> edgeType : INCOMING_EDGE_VECTOR) {
				vector[i++] = incomingEdges.contains(edgeType) ? 1 : 0;
			}
			for (Class<? extends DefaultEdge> edgeType : OUTGOING_EDGE_VECTOR) { //TODO: optimize into single loop with the above (left like this for debugging)
				vector[i++] = outgoingEdges.contains(edgeType) ? 1 : 0;
			}
			vector[i++] = node.isAbstractType() ? 1 : 0;
			vector[i++] = node.isInvokesAbstractMethods() ? 1 : 0;
			vector[i++] = node.isPublicType() ? 1 : 0;
			vector[i++] = node.isAnonymous() ? 5 : 0;

			result.put(className, vector);
		}
		return result;
	}

	private static int getMaxNodeDistance() {
		return INCOMING_EDGE_VECTOR.length + OUTGOING_EDGE_VECTOR.length + 3 + 1 * 5;
	}

	private static Set<Class<? extends DefaultEdge>> getEdgeTypes(Collection<DefaultEdge> edges) {
		Set<Class<? extends DefaultEdge>> edgeTypes = Sets.newHashSet();
		for (DefaultEdge edge : edges) {
			edgeTypes.add(EDGE_TYPE_MAP.get(edge.getClass()));
		}
		return edgeTypes;
	}

	// proposed by Gupta et al
//	private static Map<String, int[]> getNodeVector(JavaGraph graph) {
//		Map<String, int[]> result = Maps.newHashMap();
//		for (String className : graph.getNodes().keySet()) {
//			Node node = graph.get(className);
//			int superTypes = 0;
//			for (DefaultEdge edge : graph.outgoingEdgesOf(node)) {
//				superTypes += edge instanceof GeneralizationEdge ? 1 : 0;
//			}
//			int childTypes = 0;
//			for (DefaultEdge edge : graph.incomingEdgesOf(node)) {
//				childTypes += edge instanceof GeneralizationEdge ? 1 : 0;
//			}
//			result.put(className, new int[] { superTypes, childTypes, graph.edgesOf(node).size() });
//		}
//		return result;
//	}

	private static double getEuclideanDistance(int[] p, int[] q) {
		assert p.length == q.length;
		double sum = 0;
		for (int i = 0; i < p.length; i++) {
			sum += Math.pow(p[i] - q[i], 2);
		}
		return Math.sqrt(sum);
	}

	/**
	 * @see <a href='http://en.wikipedia.org/wiki/Manhattan_distance'>Manhattan distance</a>
	 * @param p
	 * @param q
	 * @return
	 */
	private static int getSimpleDistance(int[] p, int[] q) {
		assert p.length == q.length;
		int sum = 0;
		for (int i = 0; i < p.length; i++) {
			sum += Math.abs(p[i] - q[i]);
		}
		return sum;
	}

	public int getPhases() {
		return phases;
	}

	public void setPhases(int phases) {
		this.phases = phases;
	}

	@Override
	public String toString() {
		return "Inexact Graph Matching (Gupta et al. 2010), phases: " + getPhases();
	}

}
