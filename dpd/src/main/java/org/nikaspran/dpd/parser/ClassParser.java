package org.nikaspran.dpd.parser;

import org.apache.commons.lang3.StringUtils;
import org.nikaspran.dpd.model.JavaGraph;
import org.nikaspran.dpd.model.Node;
import org.nikaspran.dpd.model.edges.CollectionEdge;
import org.nikaspran.dpd.model.edges.FieldEdge;
import org.nikaspran.dpd.model.edges.InheritanceEdge;
import org.nikaspran.dpd.model.edges.InterfaceEdge;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

/**
 * Base class parser designed to connect fields, superclasses and simple method calls
 * @author Nikas
 */
public class ClassParser extends ClassVisitor {
	private final JavaGraph nodeContext;
	private String className;

	public ClassParser(JavaGraph nodeContext) {
		super(Opcodes.ASM4);
		this.nodeContext = nodeContext;
	}

	@Override
	public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
		super.visit(version, access, name, signature, superName, interfaces);

		className = ParserUtils.getClassName(name);
		Node node = nodeContext.createOrGet(className);
		node.setAbstractType((access & Opcodes.ACC_ABSTRACT) != 0);
		node.setPublicType((access & Opcodes.ACC_PUBLIC) != 0);
		node.setAnonymous(className.contains("$"));
		nodeContext.connect(className, ParserUtils.getClassName(superName), new InheritanceEdge());
		for (String interfaceName : interfaces) {
			nodeContext.connect(className, ParserUtils.getClassName(interfaceName), new InterfaceEdge());
		}
	}

	@Override
	public FieldVisitor visitField(int access, String name, String desc, String signature, Object value) {
		String typeName = Type.getType(desc).getClassName();

		boolean isCollection = false;
		if (StringUtils.endsWith(typeName, "[]")) {
			typeName = StringUtils.removeEnd(typeName, "[]");
			isCollection = true;
		} else if (StringUtils.isNotBlank(signature)) {
			typeName = StringUtils.substringAfter(Type.getType(signature).getClassName(), "<L");
			isCollection = true;
		}

		nodeContext.get(className).getFields().put(name, new FieldDescriptor(name, typeName, (access & Opcodes.ACC_PRIVATE) != 0, isCollection));
		return null;
	}

	@Override
	public void visitEnd() {
		for (FieldDescriptor field : nodeContext.get(className).getFields().values()) {
			FieldEdge edge = field.isCollection() ? new CollectionEdge(field.getName()) : new FieldEdge(field.getName());
			nodeContext.connect(className, field.getType(), edge);
		}
	}

	@Override
	public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
		MethodDescriptor method = new MethodDescriptor(name, false);
		if ((access & Opcodes.ACC_ABSTRACT) != 0) {
			method.setAbstractMethod(true);
		}
		if ((access & Opcodes.ACC_PUBLIC) != 0) {
			Node thisClass = nodeContext.get(className);
			thisClass.setPublicMethodCount(thisClass.getPublicMethodCount() + 1);
		}
		nodeContext.get(className).getMethods().put(desc + "_" + name, method);
		return new MethodParser(nodeContext, className, name);
	}

}
