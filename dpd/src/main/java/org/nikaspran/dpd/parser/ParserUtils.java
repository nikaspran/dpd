package org.nikaspran.dpd.parser;

public final class ParserUtils {

	public static String getClassName(String slashedName) {
		return slashedName.replace('/', '.');
	}

	public static boolean isConstructor(String name) {
		return "<init>".equals(name);
	}

	private ParserUtils() {
	}
}
