package org.nikaspran.dpd.parser;

import org.jgrapht.graph.DefaultEdge;
import org.nikaspran.dpd.model.Node;

public class EdgeDescriptor {
	private final Node source;
	private final Node target;
	private final DefaultEdge edge;

	public EdgeDescriptor(Node source, Node target, DefaultEdge edge) {
		this.source = source;
		this.target = target;
		this.edge = edge;
	}

	public Node getSource() {
		return source;
	}

	public Node getTarget() {
		return target;
	}

	public DefaultEdge getEdge() {
		return edge;
	}
}
