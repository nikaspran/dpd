package org.nikaspran.dpd.parser;

public class MethodDescriptor {

	private String name;
	private boolean abstractMethod;

	public MethodDescriptor(String name, boolean abstractMethod) {
		super();
		this.name = name;
		this.abstractMethod = abstractMethod;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isAbstractMethod() {
		return abstractMethod;
	}

	public void setAbstractMethod(boolean abstractMethod) {
		this.abstractMethod = abstractMethod;
	}

}
