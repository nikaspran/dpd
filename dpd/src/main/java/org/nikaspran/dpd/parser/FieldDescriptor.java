package org.nikaspran.dpd.parser;

public class FieldDescriptor {

	private String name;
	private String type;
	private boolean contained;
	private boolean collection;

	public FieldDescriptor(String name, String type, boolean contained, boolean collection) {
		super();
		if ("java.util.Vector".equals(name) || "java.util.List".equals(name)) {
			this.name = "java.util.Collection";
			this.collection = true;
		} else {
			this.name = name;
			this.collection = collection;
		}

		this.type = type;
		this.contained = contained;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isContained() {
		return contained;
	}

	public void setContained(boolean contained) {
		this.contained = contained;
	}

	public boolean isCollection() {
		return collection;
	}

	public void setCollection(boolean collection) {
		this.collection = collection;
	}

	@Override
	public String toString() {
		return "FieldDescriptor [name=" + name + ", type=" + type + ", contained=" + contained + ", collection=" + collection + "]";
	}

}
