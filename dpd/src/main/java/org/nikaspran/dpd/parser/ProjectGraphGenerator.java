package org.nikaspran.dpd.parser;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.jgrapht.graph.DefaultEdge;
import org.nikaspran.dpd.model.JavaGraph;
import org.nikaspran.dpd.model.Node;
import org.nikaspran.dpd.model.edges.GeneralizationEdge;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;

import com.google.common.collect.Sets;

public class ProjectGraphGenerator {

	public static JavaGraph generate(String jarPath) throws IOException {
		JavaGraph result = new JavaGraph(FilenameUtils.getBaseName(jarPath));

		JarFile jar = new JarFile(jarPath);
		try {
			parseJarWithClassVisitor(jar, result, new ClassParser(result)); //Connect base graph
			connectInheritanceChains(result); //Fix inheritance relationships (connect through)
			parseJarWithClassVisitor(jar, result, new ImplementorClassParser(result)); //Connect abstract classes with their implementors via association if required
		} finally {
			IOUtils.closeQuietly(jar);
		}

		return result;
	}

	private static void parseJarWithClassVisitor(JarFile jar, JavaGraph graph, ClassVisitor visitor) throws IOException {
		Enumeration<JarEntry> entries = jar.entries();
		while (entries.hasMoreElements()) {
			JarEntry entry = entries.nextElement();
			if (FilenameUtils.isExtension(entry.getName(), "class")) {
				ClassReader reader = new ClassReader(jar.getInputStream(entry));
				reader.accept(visitor, 0);
			}
		}
	}

	private static void connectInheritanceChains(JavaGraph graph) {
		Set<EdgeDescriptor> edgesToAdd = Sets.newHashSet();
		for (Node node : graph.getNodes().values()) {
			addGeneralizationEdges(node, node, graph, edgesToAdd, new HashSet<Node>());
		}
		for (EdgeDescriptor edgeToAdd : edgesToAdd) {
			graph.addEdge(edgeToAdd.getSource(), edgeToAdd.getTarget(), edgeToAdd.getEdge());
		}
	}

	private static void addGeneralizationEdges(Node source, Node current, JavaGraph graph, Set<EdgeDescriptor> edgesToAdd, Set<Node> visited) {
		visited.add(current);
		for (DefaultEdge edge : graph.outgoingEdgesOf(current)) {
			Node target = graph.getEdgeTarget(edge);
			if (edge instanceof GeneralizationEdge && graph.get(source, target, edge.getClass()) == null) {
				edgesToAdd.add(new EdgeDescriptor(source, target, (DefaultEdge) edge.clone()));
				if (!visited.contains(target)) {
					addGeneralizationEdges(source, target, graph, edgesToAdd, visited);
				}
			}
		}
	}
}
