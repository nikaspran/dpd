package org.nikaspran.dpd.parser;

import org.nikaspran.dpd.model.JavaGraph;
import org.nikaspran.dpd.model.edges.AssociationEdge;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class MethodParser extends MethodVisitor {
	private final JavaGraph nodeContext;
	private final String className;
	private final String methodName;

	public MethodParser(JavaGraph nodeContext, String className, String methodName) {
		super(Opcodes.ASM4);
		this.nodeContext = nodeContext;
		this.className = className;
		this.methodName = methodName;
	}

	@Override
	public void visitFieldInsn(int opcode, String owner, String name, String desc) {
		super.visitFieldInsn(opcode, owner, name, desc);
	}

	@Override
	public void visitMethodInsn(int opcode, String owner, String name, String desc) {
		if (ParserUtils.isConstructor(methodName) && ParserUtils.isConstructor(name)) { //TODO:check if actually calling super()
			return;
		}

		String calleeClassName = ParserUtils.getClassName(owner);
		if (!className.equals(calleeClassName)) {
			nodeContext.connectIfNot(className, calleeClassName, new AssociationEdge(name));
		}
	}
}
