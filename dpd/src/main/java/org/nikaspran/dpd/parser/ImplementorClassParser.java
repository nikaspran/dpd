package org.nikaspran.dpd.parser;

import java.util.Set;

import org.jgrapht.graph.DefaultEdge;
import org.nikaspran.dpd.model.JavaGraph;
import org.nikaspran.dpd.model.Node;
import org.nikaspran.dpd.model.edges.AssociationEdge;
import org.nikaspran.dpd.model.edges.GeneralizationEdge;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import com.google.common.collect.Sets;

/**
 * Class parser designed to connect abstract classes that call abstract methods with their implementors
 * @author Nikas
 */
public class ImplementorClassParser extends ClassVisitor {
	private final JavaGraph nodeContext;
	private String className;

	public ImplementorClassParser(JavaGraph nodeContext) {
		super(Opcodes.ASM4);
		this.nodeContext = nodeContext;
	}

	@Override
	public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
		className = ParserUtils.getClassName(name);
		super.visit(version, access, name, signature, superName, interfaces);
	}

	@Override
	public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
		return new ImplementorMethodVisitor();
	}

	private class ImplementorMethodVisitor extends MethodVisitor {

		public ImplementorMethodVisitor() {
			super(Opcodes.ASM4);
		}

		@Override
		public void visitMethodInsn(int opcode, String owner, String name, String desc) {
			Set<EdgeDescriptor> edgesToAdd = Sets.newHashSet();
			String calleeClassName = ParserUtils.getClassName(owner);
			Node thisClass = nodeContext.get(className);
			if (Opcodes.INVOKEVIRTUAL == opcode && calleeClassName.equals(className)) {
				MethodDescriptor method = thisClass.getMethods().get(desc + "_" + name);
				if (method == null) {
					// calling parent method
					for (DefaultEdge edge : nodeContext.outgoingEdgesOf(thisClass)) {
						Node target = nodeContext.getEdgeSource(edge);
						MethodDescriptor superMethod = target.getMethods().get(desc + "_" + name);
						if (edge instanceof GeneralizationEdge && superMethod != null && !superMethod.isAbstractMethod()) {
							edgesToAdd.add(new EdgeDescriptor(thisClass, target, new AssociationEdge(name)));
						}
					}
				} else if (method.isAbstractMethod()) {
					// calling abstract method
					for (DefaultEdge edge : nodeContext.incomingEdgesOf(thisClass)) {
						Node target = nodeContext.getEdgeSource(edge);
						if (edge instanceof GeneralizationEdge && !target.isAbstractType()) {
							thisClass.setInvokesAbstractMethods(true);
							edgesToAdd.add(new EdgeDescriptor(thisClass, target, new AssociationEdge(name)));
						}
					}
				}
			}

			for (EdgeDescriptor edgeToAdd : edgesToAdd) {
				nodeContext.connectIfNot(edgeToAdd.getSource().getName(), edgeToAdd.getTarget().getName(), edgeToAdd.getEdge());
			}
			super.visitMethodInsn(opcode, owner, name, desc);
		}
	}

}
