package org.nikaspran.dpd;

import java.awt.Dimension;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import org.apache.commons.io.FilenameUtils;
import org.jgraph.JGraph;
import org.nikaspran.dpd.detection.impl.InexactGraphMatchingAlgorithm;
import org.nikaspran.dpd.detection.impl.PatternMapping;
import org.nikaspran.dpd.display.JavaGraphAdapter;
import org.nikaspran.dpd.model.JavaGraph;
import org.nikaspran.dpd.parser.ProjectGraphGenerator;
import org.nikaspran.dpd.utils.AppTimer;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jgraph.layout.JGraphFacade;
import com.jgraph.layout.JGraphLayout;
import com.jgraph.layout.hierarchical.JGraphHierarchicalLayout;

public class App extends JFrame {
	//@formatter:off
	private static final long serialVersionUID = -8432695178129079306L;
	private static final Dimension DEFAULT_SIZE = new Dimension(1024, 768);
	private static boolean DRAW_GRAPH = false; /*
	private static boolean DRAW_GRAPH = true; //*/
	private static final double THRESHOLD = 0.85;
	private static final int PHASES = 20;
	private static final MappingValidityTestTypeEnum TEST = MappingValidityTestTypeEnum.EDGE;

	private static final List<String> PATTERNS = ImmutableList.<String>builder()
			.add("C:/dev/java/workspace/factory-method.jar")
			.add("C:/dev/java/workspace/prototype.jar")
			.add("C:/dev/java/workspace/singleton.jar")
			.add("C:/dev/java/workspace/adapter-obj.jar")
			.add("C:/dev/java/workspace/composite.jar")
			.add("C:/dev/java/workspace/decorator.jar")
			.add("C:/dev/java/workspace/proxy.jar")
			.add("C:/dev/java/workspace/command.jar")
			.add("C:/dev/java/workspace/iterator.jar")
			.add("C:/dev/java/workspace/observer.jar")
			.add("C:/dev/java/workspace/strategy.jar")
			.add("C:/dev/java/workspace/template-method.jar")
			.add("C:/dev/java/workspace/visitor.jar")
//			.add("C:/dev/java/workspace/facade.jar")
//			.add("C:/dev/java/workspace/adapter-cls.jar")
			.build();
	
	private static final List<String> SYSTEMS = ImmutableList.<String>builder()
			.add("C:/dev/git-repos/dpd/dpd/dpd-targets/huston.jar")
			.add("C:/dev/git-repos/dpd/dpd/dpd-targets/junit-3.7.jar")
			.add("C:/dev/git-repos/dpd/dpd/dpd-targets/jhotdraw.jar")
			.add("C:/dev/git-repos/dpd/dpd/dpd-targets/jrefactory.jar")
			.build();
	//@formatter:on

	private App() {
		try {
			Map<String, List<Long>> patternTimes = Maps.newHashMap();
			for (String pattern : PATTERNS) {
				patternTimes.put(pattern, new ArrayList<Long>());
			}

			AppTimer timer = new AppTimer();
			for (String system : SYSTEMS) {
				timer.split();
				JavaGraph systemGraph = ProjectGraphGenerator.generate(system);
				timer.logSplit("System -> graph: " + FilenameUtils.getBaseName(system));
				for (String pattern : PATTERNS) {
					timer.split();
					JavaGraph patternGraph = ProjectGraphGenerator.generate(pattern);
					patternTimes.get(pattern).add(timer.getSinceLast());

					detectPatterns(patternGraph, systemGraph);
				}
			}

			for (String pattern : patternTimes.keySet()) {
				System.out.println(FilenameUtils.getBaseName(pattern) + " => graph: " + getAverage(patternTimes.get(pattern)) + " µs");
			}

			if (!DRAW_GRAPH) {
				System.exit(0);
			}
//			drawGraph(ProjectGraphGenerator.generate(PATTERNS.get(0)));
			drawGraph(ProjectGraphGenerator.generate(SYSTEMS.get(0)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		setSize(DEFAULT_SIZE);
		setLocation(0, 150);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}

	private void detectPatterns(JavaGraph pattern, JavaGraph system) throws IOException {
		System.out.println("======================================================================================");
		System.out.println(pattern.getName() + " === " + system.getName());
		AppTimer timer = new AppTimer();

		InexactGraphMatchingAlgorithm algorithm = new InexactGraphMatchingAlgorithm(system);
		algorithm.setPhases(PHASES);
		timer.split();
		Collection<PatternMapping> mappings = algorithm.run(pattern);
		timer.logSplit("Algorithm");
		timer.stop();

//		List<PatternMapping> asList = Lists.newArrayList(mappings);
//		Collections.sort(asList, new Comparator<PatternMapping>() {
//			@Override
//			public int compare(PatternMapping o1, PatternMapping o2) {
//				int edgeError = Double.compare(o1.getEdgeError(), o2.getEdgeError());
//				return edgeError != 0 ? edgeError : Double.compare(o1.getNodeError(), o2.getNodeError());
//			}
//		});

//		for (int i = 0; i < 10; i++) {
//			System.out.println(asList.get(i));
//		}

		List<PatternMapping> foundMappings = Lists.newArrayList();
		for (PatternMapping mapping : mappings) {
			if (isUnderThreshold(mapping)) {
				foundMappings.add(mapping);
			}
		}
		System.out.println("Found " + foundMappings.size() + " mappings");
		for (PatternMapping mapping : foundMappings) {
			System.out.println(mapping.toString());
		}
	}

	private static boolean isUnderThreshold(PatternMapping mapping) {
		switch (TEST) {
		case AVERAGE:
			return mapping.getAverageError() <= 1 - THRESHOLD;
		case BOTH:
			return mapping.getEdgeError() <= 1 - THRESHOLD && mapping.getNodeError() < 1 - THRESHOLD;
		case EDGE:
			return mapping.getEdgeError() <= 1 - THRESHOLD;
		default:
			throw new IllegalStateException("Test type is not implemented: " + TEST);
		}
	}

	private void drawGraph(JavaGraph javaGraph) {
		JGraph graph = new JGraph(new JavaGraphAdapter(javaGraph));
		setGraphProperties(graph);
		add(new JScrollPane(graph));
		layoutGraph(graph, getGraphLayout());
	}

	private void layoutGraph(JGraph graph, JGraphLayout layout) {
		JGraphFacade facade = new JGraphFacade(graph);
		layout.run(facade);
		graph.getGraphLayoutCache().edit(facade.createNestedMap(true, true));
	}

	private JGraphLayout getGraphLayout() {
		JGraphHierarchicalLayout layout = new JGraphHierarchicalLayout();
		return layout;
	}

	private void setGraphProperties(final JGraph graph) {
		graph.setSize(DEFAULT_SIZE);

		graph.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				double scaleChange = -e.getWheelRotation() * 0.05;
				graph.setScale(graph.getScale() * (scaleChange + 1));
			}
		});
	}

	private static double getAverage(Collection<Long> collection) {
		long sum = 0;
		for (Long number : collection) {
			sum += number;
		}
		return sum / (double) collection.size();
	}

	public static void main(String[] args) throws IOException {
		System.setProperty("java.util.Arrays.useLegacyMergeSort", "true"); //For use with JGraphHierarchicalLayout (uses a weird comparator that is unsupported by Java 7)
		if (!DRAW_GRAPH) {
			new App();
			return;
		}
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new App().setVisible(true);
			}
		});
	}
}
