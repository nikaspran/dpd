package org.nikaspran.dpd;

public enum MappingValidityTestTypeEnum {
	EDGE,
	AVERAGE,
	BOTH;
}
