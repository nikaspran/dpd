package org.nikaspran.dpd.utils;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Stopwatch;

public class AppTimer {
	private final Stopwatch stopwatch = new Stopwatch();
	private long lastTime = 0;

	public AppTimer() {
		stopwatch.start();
	}

	public void logTime(String message) {
		stopwatch.stop();
		long time = getCurrent();
		System.out.println(String.format("[Total: %4d ms; last: %4d ms] %s", time / 1000, (time - lastTime) / 1000, ObjectUtils.defaultIfNull(message, StringUtils.EMPTY)));
		lastTime = time;
		stopwatch.start();
	}

	public void logSplit(String message) {
		stopwatch.stop();
		long time = getCurrent();
		System.out.println(String.format("[%4d ms] %s", (time - lastTime) / 1000, ObjectUtils.defaultIfNull(message, StringUtils.EMPTY)));
		lastTime = time;
		stopwatch.start();
	}

	public long getSinceLast() {
		return getCurrent() - lastTime;
	}

	public void split() {
		lastTime = getCurrent();
	}

	public void stop() {
		stopwatch.stop();
	}

	private long getCurrent() {
		return stopwatch.elapsed(TimeUnit.MICROSECONDS);
	}
}
