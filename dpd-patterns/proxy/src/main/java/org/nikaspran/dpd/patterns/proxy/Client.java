package org.nikaspran.dpd.patterns.proxy;

public class Client {

	public void clientOperation() {
		Proxy proxy = new Proxy();
		proxy.doAction();
	}

}
