package org.nikaspran.dpd.patterns.proxy;

public class Proxy implements Subject {

	private RealSubject realSubject;

	@Override
	public void doAction() {
		if (realSubject == null) {
			realSubject = new RealSubject();
		}

		realSubject.doAction();
	}

	public RealSubject getRealSubject() {
		return realSubject;
	}

	public void setRealSubject(RealSubject realSubject) {
		this.realSubject = realSubject;
	}

}
