package org.nikaspran.dpd.patterns.proxy;

public interface Subject {
	void doAction();
}
