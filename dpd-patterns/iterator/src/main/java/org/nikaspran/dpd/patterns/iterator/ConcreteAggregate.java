package org.nikaspran.dpd.patterns.iterator;

import java.util.Vector;

public class ConcreteAggregate implements Aggregate {

	private final Vector items = new Vector();

	@Override
	public Iterator createIterator() {
		return new ConcreteIterator(this);
	}

	public Vector getItems() {
		return items;
	}

}
