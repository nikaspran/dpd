package org.nikaspran.dpd.patterns.iterator;

public class ConcreteIterator implements Iterator {

	private final ConcreteAggregate aggregate;
	private int index = 0;

	public ConcreteIterator(ConcreteAggregate aggregate) {
		super();
		this.aggregate = aggregate;
	}

	@Override
	public void first() {
		index = 0;
	}

	@Override
	public void next() {
		index++;
	}

	@Override
	public boolean isDone() {
		return index == aggregate.getItems().size();
	}

	@Override
	public Object currentItem() {
		return aggregate.getItems().get(index);
	}

}
