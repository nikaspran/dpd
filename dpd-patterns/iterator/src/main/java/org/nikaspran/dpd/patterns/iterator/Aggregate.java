package org.nikaspran.dpd.patterns.iterator;

public interface Aggregate {

	public Iterator createIterator();

}
