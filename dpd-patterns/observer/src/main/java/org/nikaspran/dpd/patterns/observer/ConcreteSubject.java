package org.nikaspran.dpd.patterns.observer;

public class ConcreteSubject extends Subject {

	private Object state = new Object();

	public Object getState() {
		return state;
	}

	public void setState(Object state) {
		this.state = state;
	}

}
