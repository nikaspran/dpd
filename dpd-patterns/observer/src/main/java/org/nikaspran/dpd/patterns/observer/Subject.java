package org.nikaspran.dpd.patterns.observer;

import java.util.Vector;

public abstract class Subject {

	private final Vector observers = new Vector();

	public void attach(Observer observer) {
		observers.add(observer);
	}

	public void detach(Observer observer) {
		observers.remove(observer);
	}

	public void notifyObservers() {
		for (Object observer : observers) {
			((Observer) observer).update(this);
		}
	}
}
