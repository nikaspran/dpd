package org.nikaspran.dpd.patterns.observer;

public interface Observer {

	public void update(Subject subject);

}
