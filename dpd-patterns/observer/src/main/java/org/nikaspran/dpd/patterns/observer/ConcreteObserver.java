package org.nikaspran.dpd.patterns.observer;

public class ConcreteObserver implements Observer {

	private final ConcreteSubject subject;

	public ConcreteObserver(ConcreteSubject subject) {
		super();
		this.subject = subject;
	}

	@Override
	public void update(Subject subject) {
		if (this.subject == subject) {
			this.subject.getState();
		}
	}

}
