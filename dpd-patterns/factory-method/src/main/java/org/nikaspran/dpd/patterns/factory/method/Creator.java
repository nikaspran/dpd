package org.nikaspran.dpd.patterns.factory.method;

public interface Creator {

	public Product factoryMethod();

}
