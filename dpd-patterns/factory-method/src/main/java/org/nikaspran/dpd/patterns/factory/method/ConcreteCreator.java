package org.nikaspran.dpd.patterns.factory.method;

public class ConcreteCreator implements Creator {

	@Override
	public Product factoryMethod() {
		return new ConcreteProduct();
	}

}
