package org.nikaspran.dpd.patterns.template.method;

public abstract class AbstractClass {

	public final void templateMethod() {
		primitiveOperation();
	}

	protected abstract void primitiveOperation();

}
