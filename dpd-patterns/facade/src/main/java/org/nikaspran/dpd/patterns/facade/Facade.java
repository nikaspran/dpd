package org.nikaspran.dpd.patterns.facade;

public class Facade {

	private SubSystem1 subSystem1;
	private SubSystem2 subSystem2;

	public void facadeMethod() {
		subSystem1.subMethod1();
		subSystem2.subMethod2();
	}

	public SubSystem1 getSubSystem1() {
		return subSystem1;
	}

	public void setSubSystem1(SubSystem1 subSystem1) {
		this.subSystem1 = subSystem1;
	}

	public SubSystem2 getSubSystem2() {
		return subSystem2;
	}

	public void setSubSystem2(SubSystem2 subSystem2) {
		this.subSystem2 = subSystem2;
	}

}
