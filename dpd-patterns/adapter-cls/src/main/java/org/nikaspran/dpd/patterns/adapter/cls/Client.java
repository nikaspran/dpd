package org.nikaspran.dpd.patterns.adapter.cls;

public class Client {

	public void operation() {
		Target target = new Adapter();
		target.request();
	}

}
