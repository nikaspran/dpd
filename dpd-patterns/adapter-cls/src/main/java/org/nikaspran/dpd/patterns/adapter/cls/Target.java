package org.nikaspran.dpd.patterns.adapter.cls;

public interface Target {
	public void request();
}
