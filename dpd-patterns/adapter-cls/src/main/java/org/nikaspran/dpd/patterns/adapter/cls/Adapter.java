package org.nikaspran.dpd.patterns.adapter.cls;

public class Adapter extends Adaptee implements Target {

	@Override
	public void request() {
		super.specificRequest();
	}

}
