package org.nikaspran.dpd.patterns.visitor;

public class ConcreteVisitor implements Visitor {

	@Override
	public void visitConcreteElement(ConcreteElement element) {
		element.operation();
	}

}
