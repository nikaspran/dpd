package org.nikaspran.dpd.patterns.visitor;

public interface Element {
	public void accept(Visitor visitor);
}
