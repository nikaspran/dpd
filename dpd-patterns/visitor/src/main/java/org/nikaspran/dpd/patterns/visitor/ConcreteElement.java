package org.nikaspran.dpd.patterns.visitor;

public class ConcreteElement implements Element {

	@Override
	public void accept(Visitor visitor) {
		visitor.visitConcreteElement(this);
	}

	public void operation() {

	}

}
