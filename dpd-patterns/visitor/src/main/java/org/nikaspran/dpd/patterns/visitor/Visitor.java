package org.nikaspran.dpd.patterns.visitor;

public interface Visitor {

	public void visitConcreteElement(ConcreteElement element);

}
