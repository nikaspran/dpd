package org.nikaspran.dpd.patterns.adapter.obj;

public class Adapter implements Target {

	private final Adaptee adaptee;

	public Adapter(Adaptee instance) {
		super();
		this.adaptee = instance;
	}

	@Override
	public void request() {
		adaptee.specificRequest();
	}

}
