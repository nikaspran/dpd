package org.nikaspran.dpd.patterns.adapter.obj;

public class Client {

	public void operation() {
		Target target = new Adapter(new Adaptee());
		target.request();
	}

}
