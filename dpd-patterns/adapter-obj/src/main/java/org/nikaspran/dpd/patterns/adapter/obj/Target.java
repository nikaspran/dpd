package org.nikaspran.dpd.patterns.adapter.obj;

public interface Target {
	public void request();
}
