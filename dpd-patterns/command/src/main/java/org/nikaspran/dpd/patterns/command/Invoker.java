package org.nikaspran.dpd.patterns.command;

public class Invoker {
	private final Command command;

	public Invoker(Command command) {
		super();
		this.command = command;
	}

	public void invoke() {
		command.execute();
	}
}
