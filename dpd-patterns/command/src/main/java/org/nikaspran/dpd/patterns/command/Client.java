package org.nikaspran.dpd.patterns.command;

public class Client {

	private final Receiver receiver = new Receiver();

	public void operation() {
		Invoker invoker = new Invoker(new ConcreteCommand(receiver));
		invoker.invoke();
	}

}
