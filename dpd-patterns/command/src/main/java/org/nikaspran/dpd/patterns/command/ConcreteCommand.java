package org.nikaspran.dpd.patterns.command;

public class ConcreteCommand implements Command {

	private final Receiver receiver;

	public ConcreteCommand(Receiver receiver) {
		super();
		this.receiver = receiver;
	}

	@Override
	public void execute() {
		receiver.action();
	}

}
