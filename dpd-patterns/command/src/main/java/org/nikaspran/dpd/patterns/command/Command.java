package org.nikaspran.dpd.patterns.command;

public interface Command {
	public void execute();
}
