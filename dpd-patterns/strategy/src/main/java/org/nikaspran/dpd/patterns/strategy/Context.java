package org.nikaspran.dpd.patterns.strategy;

public class Context {
	private final Strategy strategy;

	public Context(Strategy strategy) {
		this.strategy = strategy;
	}

	public void executeStrategy() {
		strategy.execute();
	}
}
