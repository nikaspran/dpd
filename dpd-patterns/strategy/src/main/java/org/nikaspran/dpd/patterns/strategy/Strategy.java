package org.nikaspran.dpd.patterns.strategy;

public interface Strategy {
	void execute();
}
