package org.nikaspran.dpd.patterns.composite;

import java.util.Vector;

public class Composite implements Component {

	private final Vector components = new Vector();

	@Override
	public void operation() {
		for (Object component : components) {
			((Component) component).operation();
		}
	}

	public void add(Component component) {
		components.add(component);
	}

	public void remove(Component component) {
		components.remove(component);
	}

}
