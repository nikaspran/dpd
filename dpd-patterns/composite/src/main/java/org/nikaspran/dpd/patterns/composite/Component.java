package org.nikaspran.dpd.patterns.composite;

public interface Component {
	void operation();
}
