package org.nikaspran.dpd.patterns.prototype;

public class Client {

	private final Prototype prototype = new ConcretePrototype();

	public Prototype operation() {
		return prototype.copy();
	}

}
