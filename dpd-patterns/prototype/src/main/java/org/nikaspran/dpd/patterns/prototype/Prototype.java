package org.nikaspran.dpd.patterns.prototype;

public interface Prototype {

	public Prototype copy();

}
