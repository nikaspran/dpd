package org.nikaspran.dpd.patterns.prototype;

public class ConcretePrototype implements Prototype {

	private Object state = new Object();

	@Override
	public Prototype copy() {
		ConcretePrototype copy = new ConcretePrototype();
		copy.setState(state);
		return copy;
	}

	public Object getState() {
		return state;
	}

	public void setState(Object state) {
		this.state = state;
	}

}
