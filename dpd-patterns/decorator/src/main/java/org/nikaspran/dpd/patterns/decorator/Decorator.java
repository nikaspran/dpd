package org.nikaspran.dpd.patterns.decorator;

public abstract class Decorator implements Component {

	private final Component component;

	public Decorator(Component component) {
		super();
		this.component = component;
	}

	@Override
	public void operation() {
		component.operation();
	}

	public Component getComponent() {
		return component;
	}

}
