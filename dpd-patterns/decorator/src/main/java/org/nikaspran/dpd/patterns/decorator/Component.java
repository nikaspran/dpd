package org.nikaspran.dpd.patterns.decorator;

public interface Component {
	void operation();
}
